package net.rappi.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.rappi.exceptions.CubeSummationException;
import net.rappi.resources.Input;
import net.rappi.resources.records.OperationsRecord;
import net.rappi.resources.records.QueryRecord;
import net.rappi.resources.records.Record;
import net.rappi.resources.records.UpdateRecord;

public class Utils {
	
	private static List<String> msjSalidaList = new ArrayList<String>();
	
	@SuppressWarnings("finally")
	public static List<String> execute(String userInput) {

		try {
			msjSalidaList.clear();
			List<String> datosEntradaList = Utils.leerEntrada(userInput);
			Input archivoLeidoInput = null;
			archivoLeidoInput = Utils.procesarArchivoLeido(datosEntradaList);
			realizarOperaciones(archivoLeidoInput);
		}
		catch(CubeSummationException e) {
			msjSalidaList.add(e.getMessage());
		}
		catch(Exception e) {
			msjSalidaList.add(e.getMessage());
		}
		finally {
			return msjSalidaList;
		}
	}
	
	protected static Input procesarArchivoLeido(List<String> datosEntradaList) throws CubeSummationException {

		Input input = new Input();
		datosEntradaList.stream()
		.forEach(de -> {
			String[] splittedLine = de.split(" ");
			int lineLength = splittedLine.length;

			switch (lineLength) {
			case 1:
				validateIfValueIsInRange(Integer.valueOf(splittedLine[0]), Constants.CASOS_MIN, Constants.CASOS_MAX, "T");
				int numberOfTestCases = Integer.valueOf(splittedLine[0]);
				input.setNumTestCases(numberOfTestCases);
				break;

			case 2: // Operations record
				validateIfValueIsInRange(Integer.valueOf(splittedLine[0]), Constants.LADO_MIN, Constants.LADO_MAX, "N");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[1]), Constants.OPERACIONES_MIN, Constants.OPERACIONES_MAX, "M");
				OperationsRecord operationsRecord = new OperationsRecord();
				operationsRecord.setLengthOfSide(Integer.valueOf(splittedLine[0]));
				operationsRecord.setNumberOfOperations(Integer.valueOf(splittedLine[1]));
				operationsRecord.setQueryType(Constants.CADENA_INFO);
				input.getOperationsRecordList().add(operationsRecord);
				input.getQueriesList().add(operationsRecord);
				break;

			case 5: // UPDATE sentence
				UpdateRecord updateRecord = new UpdateRecord();
				validateIfValueIsInRange(Integer.valueOf(splittedLine[1]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "X");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[2]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Y");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[3]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Z");
				int xCoordinate = Integer.valueOf(splittedLine[1]);
				int yCoordinate = Integer.valueOf(splittedLine[2]);
				int zCoordinate = Integer.valueOf(splittedLine[3]);
				int value = Integer.valueOf(splittedLine[4]);
				updateRecord.setX(xCoordinate);
				updateRecord.setY(yCoordinate);
				updateRecord.setZ(zCoordinate);
				updateRecord.setValue(value);
				updateRecord.setQueryType(Constants.CADENA_UPDATE);
				input.getQueriesList().add(updateRecord);
				break;

			case 7: // QUERY sentence
				QueryRecord queryRecord = new QueryRecord();
				validateIfValueIsInRange(Integer.valueOf(splittedLine[1]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "X1");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[2]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Y1");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[3]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Z1");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[4]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "X2");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[5]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Y2");
				validateIfValueIsInRange(Integer.valueOf(splittedLine[6]), Constants.COORDENADAS_MIN, Constants.LADO_MAX, "Z2");
				int x1Coordinate = Integer.valueOf(splittedLine[1]);
				int y1Coordinate = Integer.valueOf(splittedLine[2]);
				int z1Coordinate = Integer.valueOf(splittedLine[3]);
				int x2Coordinate = Integer.valueOf(splittedLine[4]);
				int y2Coordinate = Integer.valueOf(splittedLine[5]);
				int z2Coordinate = Integer.valueOf(splittedLine[6]);
				queryRecord.setX1(x1Coordinate);
				queryRecord.setY1(y1Coordinate);
				queryRecord.setZ1(z1Coordinate);
				queryRecord.setX2(x2Coordinate);
				queryRecord.setY2(y2Coordinate);
				queryRecord.setZ2(z2Coordinate);
				queryRecord.setQueryType(Constants.CADENA_QUERY);
				input.getQueriesList().add(queryRecord);
				break;

			default:
				System.out.println("TIPO DE REGISTRO NO IDENTIFICADO");
				break;
			}
		});
		return input;
	}
	
	protected static void validateIfValueIsInRange(int testValue, int minValue, int maxValue, String variable) {
		if(!(testValue >= minValue && testValue <= maxValue)) {
			throw new CubeSummationException("El valor de " + variable +  " esta fuera del rango permitido");

		}
	}
	
	protected static List<String> leerEntrada(String userInput) {

		String lines[] = userInput.split("\\r?\\n");
		
		List<String> inputList = new ArrayList<>(Arrays.asList(lines));

		return inputList;

	}
	
	private static void realizarOperaciones(Input input) throws CubeSummationException {

		List<Integer> indicesInfoList = obtenerIndicesInfo(input.getQueriesList());
		if(input.getNumTestCases() != input.getOperationsRecordList().size()) 
			throw new CubeSummationException("El numero de casos de prueba del archivo no corresponde con los registrados en el archivo");
		// Numero de casos de prueba
		for (int i = 0; i < input.getNumTestCases(); i++) {
			OperationsRecord operationsRecord = input.getOperationsRecordList().get(i);
			int lengthOfSide = operationsRecord.getLengthOfSide();
			int numberOfOperations = operationsRecord.getNumberOfOperations();
			int[][][] cube = new int[lengthOfSide][lengthOfSide][lengthOfSide];
			int firstIndex = indicesInfoList.get(i) + 1;
			int lastIndex = (firstIndex + numberOfOperations) < input.getQueriesList().size() ? (firstIndex + numberOfOperations) : input.getQueriesList().size();
			
			operationsLoop:for (int j = firstIndex; j < lastIndex; j++) {
				String queryType = input.getQueriesList().get(j).getQueryType();
				switch (queryType) {
				case Constants.CADENA_QUERY:
					msjSalidaList.add(computeSum(input.getQueriesList().get(j), cube));
					break;
				case Constants.CADENA_UPDATE:
					modifyCube(input.getQueriesList().get(j), cube);
					break;
				case Constants.CADENA_INFO:
					break operationsLoop;
				}
			}				

		}
	}
	
	private static List<Integer> obtenerIndicesInfo(List<Record> queriesList) {

		List<Integer> indicesList = new ArrayList<>();

		for (int i = 0; i < queriesList.size(); i++) {
			if(queriesList.get(i).getQueryType().equals(Constants.CADENA_INFO)) {
				indicesList.add(i);
			}
		}
		return indicesList;
	}

	protected static void modifyCube(Record record, int[][][] cube) {
		UpdateRecord updateRecord = (UpdateRecord) record;
		int xCoordinate = updateRecord.getX() - 1;
		int yCoordinate = updateRecord.getY() - 1;
		int zCoordinate = updateRecord.getZ() - 1;
		validateIfCoordinatesAreInRange(xCoordinate,yCoordinate, zCoordinate, cube.length);
		int value = updateRecord.getValue();
		cube[xCoordinate][yCoordinate][zCoordinate] = value;
	}

	protected static String computeSum(Record record, int[][][] cube) {
		QueryRecord queryRecord = (QueryRecord) record;
		int xInicial = queryRecord.getX1() - 1;
		int yInicial = queryRecord.getY1() - 1;
		int zInicial = queryRecord.getZ1() - 1;
		int xFinal = queryRecord.getX2() - 1;
		int yFinal = queryRecord.getY2() - 1;
		int zFinal = queryRecord.getZ2() - 1;
		int suma = 0;
		
		validateIfCoordinatesAreInRange(xInicial,yInicial, zInicial, cube.length);
		validateIfCoordinatesAreInRange(xFinal,yFinal, zFinal, cube.length);
		
		for (int i = xInicial; i <= xFinal; i++)
			for (int j = yInicial; j <= yFinal; j++)
				for (int k = zInicial; k <= zFinal; k++) {
					suma += cube[i][j][k];
				}
		return String.valueOf(suma);
	}
	
	protected static void validateIfCoordinatesAreInRange(int x, int y, int z, int maxValue) {
		
		if(x >= maxValue || y >= maxValue || z >= maxValue) {
			int sumX = x + 1;
			int sumY = y + 1;
			int sumZ = z + 1;
			throw new CubeSummationException("Coordenadas " + sumX + ", " + sumY + ", " + sumZ + " no son validas");
		}
	}
	
}
