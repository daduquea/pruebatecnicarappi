/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rappi.business;

/**
 *
 * @author david
 */
public class Constants {
    
    public static final String CADENA_QUERY = "QUERY";
    public static final String CADENA_UPDATE = "UPDATE";
    public static final String CADENA_INFO = "INFO";
    public static final Integer LADO_MIN = 1;
    public static final Integer LADO_MAX = 100;
    public static final Integer CASOS_MIN = 1;
    public static final Integer CASOS_MAX = 50;
    public static final Integer OPERACIONES_MIN = 1;
    public static final Integer OPERACIONES_MAX = 1000;
    public static final Integer COORDENADAS_MIN = 1;
    public static final Integer VALUE_MIN = -1000000000;
    public static final Integer VALUE_MAX = -1000000000;
    
    
}
