package net.rappi.exceptions;

public class CubeSummationException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -600672906219698615L;

	public CubeSummationException() {}
	
    public CubeSummationException(String message)
    {
       super(message);
    }

}
