
package net.rappi.resources.records;

public class OperationsRecord extends Record {

	private int lengthOfSide;
	private int numberOfOperations;
	
	public int getNumberOfOperations() {
		return numberOfOperations;
	}
	public void setNumberOfOperations(int numberOfOperations) {
		this.numberOfOperations = numberOfOperations;
	}
	public int getLengthOfSide() {
		return lengthOfSide;
	}
	public void setLengthOfSide(int lengthOfSide) {
		this.lengthOfSide = lengthOfSide;
	}
}