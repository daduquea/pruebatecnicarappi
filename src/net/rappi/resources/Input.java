
package net.rappi.resources;

import java.util.ArrayList;
import java.util.List;

import net.rappi.resources.records.OperationsRecord;
import net.rappi.resources.records.Record;

public class Input {

	private int numTestCases;
	private List<Record> queriesList = new ArrayList<>();
	private List<OperationsRecord> operationsRecordList = new ArrayList<>();
	
	public int getNumTestCases() {
		return numTestCases;
	}
	public void setNumTestCases(int numTestCases) {
		this.numTestCases = numTestCases;
	}
	public List<OperationsRecord> getOperationsRecordList() {
		return operationsRecordList;
	}
	public void setOperationsRecordList(List<OperationsRecord> operationsRecordList) {
		this.operationsRecordList = operationsRecordList;
	}
	public List<Record> getQueriesList() {
		return queriesList;
	}
	public void setQueriesList(List<Record> queriesList) {
		this.queriesList = queriesList;
	}
	
}