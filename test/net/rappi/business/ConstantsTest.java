package net.rappi.business;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

import net.rappi.business.Constants;

public class ConstantsTest {

	@Test
	public void test() {
		assertThat(Constants.CADENA_INFO, is("INFO"));
		assertThat(Constants.CADENA_QUERY, is("QUERY"));
		assertThat(Constants.CADENA_UPDATE, is("UPDATE"));
	}

}
