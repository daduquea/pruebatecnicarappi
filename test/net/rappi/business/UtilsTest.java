package net.rappi.business;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import net.rappi.business.Constants;
import net.rappi.business.Utils;
import net.rappi.resources.Input;
import net.rappi.resources.records.OperationsRecord;
import net.rappi.resources.records.QueryRecord;
import net.rappi.resources.records.Record;
import net.rappi.resources.records.UpdateRecord;

public class UtilsTest {



	@Test
	public void leerEntrada() {
		String fileContent = "1\n" + 
				"3 2\n" + 
				"UPDATE 1 2 3 4\n" + 
				"QUERY 1 1 1 3 3 3";
		List<String> inputDataList = Utils.leerEntrada(fileContent);
		String line1 = inputDataList.get(0);
		String line2 = inputDataList.get(1);
		String line3 = inputDataList.get(2);
		String line4 = inputDataList.get(3);
		assertThat(line1, is("1"));
		assertThat(line2, is("3 2"));
		assertThat(line3, is("UPDATE 1 2 3 4"));
		assertThat(line4, is("QUERY 1 1 1 3 3 3"));
	}
	
	
	
	@Test
	public void procesarArchivoLeido() {
		String fileContent = "1\n" + 
				"3 2\n" + 
				"UPDATE 1 2 3 4\n" + 
				"QUERY 1 1 1 3 3 3";
		List<String> inputDataList = Utils.leerEntrada(fileContent);
		Input readFileInput = Utils.procesarArchivoLeido(inputDataList);
		
		// Just 1 test case in test.txt
		assertThat(readFileInput.getNumTestCases(), is(1));
		
		OperationsRecord operationsRecord = readFileInput.getOperationsRecordList().get(0);
		// Length of the cube side is 3
		assertThat(operationsRecord.getLengthOfSide(), is(3));
		
		// Number of operations in test case is 2
		assertThat(operationsRecord.getNumberOfOperations(), is(2));

		List<Record> queriesList = readFileInput.getQueriesList();
		// First operation is Update
		UpdateRecord updateRecord = (UpdateRecord)queriesList.get(1);
		assertThat(updateRecord.getQueryType(),is(Constants.CADENA_UPDATE));
		
		// Coordinates of update is x=1, y=2, z=3, value = 4
		assertThat(updateRecord.getX(), is(1));
		assertThat(updateRecord.getY(), is(2));
		assertThat(updateRecord.getZ(), is(3));
		assertThat(updateRecord.getValue(), is(4));
		
		// Second operation is Query 
		
		QueryRecord queryRecord = (QueryRecord)queriesList.get(2);
		
		// Coordinates of query is x1 = y1 = z1 = 1 and x2 = y2 = z2 = 3
		assertThat(queryRecord.getX1(), is(1));
		assertThat(queryRecord.getY1(), is(1));
		assertThat(queryRecord.getZ1(), is(1));
		assertThat(queryRecord.getX2(), is(3));
		assertThat(queryRecord.getY2(), is(3));
		assertThat(queryRecord.getZ2(), is(3));
	}
	
	
}
