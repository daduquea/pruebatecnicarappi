# README #


### What is this repository for? ###

This repository has been created in order to manage version control of the tecnical challenge.

### How do I get set up? ###
If you want to deploy Web application:
	Download the project "PruebaTecnicaWeb". It was developed using Servlets and Junit. It runs in Java 8.
	Deploy the Web Application and access to it through a browser using: http://localhost:8080/PruebaTecnicaWeb/
	Type on the text box the input for cube cummation.
	Try it!

If you want to try Unit Testing using Junit:
	Open any package from the package net.rappi.business
	Right click on the class > Run as > JUnit test
	Try it!

### Who do I talk to? ###
This code was developed by David Arturo Duque Arias